### Подключение пакетов

    library(arrow)

    ## Warning: пакет 'arrow' был собран под R версии 4.2.3

    ## The tzdb package is not installed. Timezones will not be available to Arrow compute functions.

    ## 
    ## Присоединяю пакет: 'arrow'

    ## Следующий объект скрыт от 'package:utils':
    ## 
    ##     timestamp

    library(dplyr)

    ## Warning: пакет 'dplyr' был собран под R версии 4.2.3

    ## 
    ## Присоединяю пакет: 'dplyr'

    ## Следующие объекты скрыты от 'package:stats':
    ## 
    ##     filter, lag

    ## Следующие объекты скрыты от 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

    library(stringr)
    library(lubridate)

    ## Warning: пакет 'lubridate' был собран под R версии 4.2.3

    ## 
    ## Присоединяю пакет: 'lubridate'

    ## Следующий объект скрыт от 'package:arrow':
    ## 
    ##     duration

    ## Следующие объекты скрыты от 'package:base':
    ## 
    ##     date, intersect, setdiff, union

    library(ggplot2)

    ## Warning: пакет 'ggplot2' был собран под R версии 4.2.3

### Импорт датасета

    dataset <- arrow::read_csv_arrow("..\\traffic_security.csv",schema = schema(timestamp=int64(),src=utf8(),dst=utf8(),port=uint32(),bytes=uint32()))

# Задание 3: Найдите утечку данных 3

### Еще один нарушитель собирает содержимое электронной почты и отправляет в Интернет используя порт, который обычно используется для другого типа трафика. Атакующий пересылает большое количество информации используя этот порт, которое нехарактерно для других хостов, использующих этот номер порта. Определите IP этой системы. Известно, что ее IP адрес отличается от нарушителей из предыдущих задач.

      general_table<-filter(dataset, src != "13.37.84.125", src != "12.55.77.96", str_detect(src,"^((12|13|14)\\.)", !str_detect(dst,"^((12|13|14)\\.)"))) %>% select(src, bytes, port)
      table_sorted <-general_table[order(general_table$port, decreasing = TRUE), ]%>%group_by(src, port)

### Суммируем по каждому ip и port количество отправленных bytes

      table_sum<-table_sorted%>%summarize(port_sum = sum(bytes))%>%group_by(port)

    ## `summarise()` has grouped output by 'src'. You can override using the `.groups`
    ## argument.

### Получаем среднее значение байт по каждому порту и мёржим с отсортированной таблицей

      table_mean<-table_sum%>%summarize(port_mean = mean(port_sum))
      
      table_merge<-merge(table_sorted, table_mean, by = "port")

### Находим минимальную разницу между количеством байт и средним значением байт по каждому порту. Чем меньше значение, тем больше данных ip отправил.

    res_ip<-filter(table_merge,str_detect(src,"13.37.84.125",negate=TRUE)&str_detect(src,"12.55.77.96",negate=TRUE))
    res_ip$difference <- res_ip$bytes - res_ip$port_mean
    min_diff_index <- which.min(res_ip$difference)

    min_diff_row <- res_ip[min_diff_index,]
      min_diff_row%>%head(1)%>%
      select(src)

    ##                   src
    ## 27962997 15.94.59.123
### Ответ: 15.94.59.123