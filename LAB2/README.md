### Подключение пакетов

    #library(rlang)
    library(arrow)

    ## Warning: пакет 'arrow' был собран под R версии 4.2.3

    ## The tzdb package is not installed. Timezones will not be available to Arrow compute functions.

    ## 
    ## Присоединяю пакет: 'arrow'

    ## Следующий объект скрыт от 'package:utils':
    ## 
    ##     timestamp

    library(dplyr)

    ## Warning: пакет 'dplyr' был собран под R версии 4.2.3

    ## 
    ## Присоединяю пакет: 'dplyr'

    ## Следующие объекты скрыты от 'package:stats':
    ## 
    ##     filter, lag

    ## Следующие объекты скрыты от 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

    library(stringr)
    library(lubridate)

    ## Warning: пакет 'lubridate' был собран под R версии 4.2.3

    ## 
    ## Присоединяю пакет: 'lubridate'

    ## Следующий объект скрыт от 'package:arrow':
    ## 
    ##     duration

    ## Следующие объекты скрыты от 'package:base':
    ## 
    ##     date, intersect, setdiff, union

    library(ggplot2)

    ## Warning: пакет 'ggplot2' был собран под R версии 4.2.3

### Импорт датасета

    dataset <- arrow::read_csv_arrow("..\\traffic_security.csv",schema = schema(timestamp=int64(),src=utf8(),dst=utf8(),port=uint32(),bytes=uint32()))

# Задание 2: Надите утечку данных 2

## Другой атакующий установил автоматическую задачу в системном планировщике cron для экспорта содержимого внутренней wiki системы. Эта система генерирует большое количество траффика в нерабочие часы, больше чем остальные хосты. Определите IP этой системы. Известно, что ее IP адрес отличается от нарушителя из предыдущей задачи.

### Графики распределения отправленных пакетов в каждый час и количества данных за каждый час

    dataset %>%
      select(timestamp, src, dst, bytes) %>%
      mutate(external_traffic = (str_detect(src, "^((12|13|14)\\.)") & !str_detect(dst, "^((12|13|14)\\.)")),hour = hour(as_datetime(timestamp/1000))) %>%
      filter(external_traffic == TRUE, hour >= 0 & hour <= 24) %>%
      group_by(hour) %>%
      summarise(packets = n()) %>%
      collect() %>%
      
      ggplot(., aes(x = hour, y = packets),) +
             geom_col(fill = "violetred", color = "black") + labs(title = "Количество пакетов за каждый час", x = "Часы", y = "Пакеты")+ theme_bw()

![](LAB2_files/figure-markdown_strict/unnamed-chunk-3-1.png)

    dataset %>%
      select(timestamp, src, dst, bytes) %>%
      mutate(external_traffic = (str_detect(src, "^((12|13|14)\\.)") & !str_detect(dst, "^((12|13|14)\\.)")),hour = hour(as_datetime(timestamp/1000))) %>%
      filter(external_traffic == TRUE, hour >= 0 & hour <= 24) %>%
    group_by(hour) %>%
    summarise(bytes = sum(bytes)) %>%
      collect() %>%
      
    ggplot(., aes(x = hour, y = bytes),) +
             geom_col(fill = "violetred", color = "black") + labs(title = "Количество данных за каждый час", x = "Часы", y = "Данные")+ theme_bw()

![](LAB2_files/figure-markdown_strict/unnamed-chunk-3-2.png)

### Построив графики распределения количества пакетов и данных за каждый час, видно, что:

      С 0:00 по 15:00 активность наименьшая по сравнению  с 16:00 по 24:00

### Следовательно нерабочее время - 0-15.

### Нужный ip-адрес: 12.55.77.96

    dataset %>%
      select(timestamp, src, dst, bytes) %>%
      filter(src != "13.37.84.125") %>%
      mutate(outside_traffic = (str_detect(src,"^((12|13|14)\\.)") & !str_detect(dst,"^((12|13|14)\\.)")), hour = hour(as_datetime(timestamp/1000))) %>%
      filter(outside_traffic == TRUE, hour >= 0 & hour <= 15) %>%
      group_by(src) %>%
      summarise(bytes = sum(bytes)) %>%
      slice_max(bytes) %>%
      select(src)

    ## # A tibble: 1 × 1
    ##   src        
    ##   <chr>      
    ## 1 12.55.77.96
