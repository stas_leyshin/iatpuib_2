---
title: "LAB4"
---


### Подключение пакетов


```{r}
    library(arrow)
    library(dplyr)
    library(stringr)
    library(ggplot2)
    library(stringr)
    library(lubridate)
```


### Импорт датасета


```{r}
    dataset <- arrow::read_csv_arrow("..\\traffic_security.csv",schema = schema(timestamp=int64(),src=utf8(),dst=utf8(),port=uint32(),bytes=uint32()))
```


# Задание 1: Найдите утечку данных из Вашей сети

Важнейшие документы с результатами нашей исследовательской деятельности в области создания вакцин скачиваются в виде больших заархивированных дампов. Один из хостов в нашей сети используется для пересылки этой информации -- он пересылает гораздо больше информации на внешние ресурсы в Интернете, чем остальные компьютеры нашей сети. Определите его IP-адрес.

### Определение IP-адреса, который пересылает больше информации на внешние ресурсы.


```{r}
filter(dataset,str_detect(src,"^((12|13|14)\\.)"),
             str_detect(dst,"^((12|13|14)\\.)",negate=TRUE)) %>% 
      select(src,bytes) %>%
      group_by(src)%>% 
      summarise(bytes=sum(bytes))%>%
      slice_max(bytes)%>%
      select(src)
```


### Ответ: 13.37.84.125

# Задание 2: Надите утечку данных 2

Другой атакующий установил автоматическую задачу в системном планировщике cron для экспорта содержимого внутренней wiki системы. Эта система генерирует большое количество траффика в нерабочие часы, больше чем остальные хосты. Определите IP этой системы. Известно, что ее IP адрес отличается от нарушителя из предыдущей задачи.

### Графики распределения отправленных пакетов в каждый час и количества данных за каждый час


```{r}
dataset %>%
      select(timestamp, src, dst, bytes) %>%
      mutate(external_traffic = (str_detect(src, "^((12|13|14)\\.)") & !str_detect(dst, "^((12|13|14)\\.)")),hour = hour(as_datetime(timestamp/1000))) %>%
      filter(external_traffic == TRUE, hour >= 0 & hour <= 24) %>%
      group_by(hour) %>%
      summarise(packets = n()) %>%
      collect() %>%
      
      ggplot(., aes(x = hour, y = packets),) +
             geom_col(fill = "violetred", color = "black") + labs(title = "Количество пакетов за каждый час", x = "Часы", y = "Пакеты")+ theme_bw()
```

```{r}
    dataset %>%
      select(timestamp, src, dst, bytes) %>%
      mutate(external_traffic = (str_detect(src, "^((12|13|14)\\.)") & !str_detect(dst, "^((12|13|14)\\.)")),hour = hour(as_datetime(timestamp/1000))) %>%
      filter(external_traffic == TRUE, hour >= 0 & hour <= 24) %>%
    group_by(hour) %>%
    summarise(bytes = sum(bytes)) %>%
      collect() %>%
      
    ggplot(., aes(x = hour, y = bytes),) +
             geom_col(fill = "violetred", color = "black") + labs(title = "Количество данных за каждый час", x = "Часы", y = "Данные")+ theme_bw()
```


### Построив графики распределения количества пакетов и данных за каждый час, видно, что:

      С 0:00 по 15:00 активность наименьшая по сравнению  с 16:00 по 24:00
      Следовательно нерабочее время - 0-15.

### Ответ: 12.55.77.96

# Задание 3: Найдите утечку данных 3

Еще один нарушитель собирает содержимое электронной почты и отправляет в Интернет используя порт, который обычно используется для другого типа трафика. Атакующий пересылает большое количество информации используя этот порт, которое нехарактерно для других хостов, использующих этот номер порта. Определите IP этой системы. Известно, что ее IP адрес отличается от нарушителей из предыдущих задач.


```{r}
      general_table<-filter(dataset, src != "13.37.84.125", src != "12.55.77.96", str_detect(src,"^((12|13|14)\\.)") & !str_detect(dst,"^((12|13|14)\\.)")) %>%
  select(src, bytes, port)
table_sorted <-general_table[order(general_table$port, decreasing = TRUE), ]%>%
  group_by(src, port)
```


### Суммируем по каждому ip и port количество отправленных bytes


```{r}
      table_sum<-table_sorted%>%summarize(port_sum = sum(bytes))%>%group_by(port)
```


### Получаем среднее значение байт по каждому порту и мёржим с отсортированной таблицей


```{r}
      table_mean<-table_sum%>%summarize(port_mean = mean(port_sum))
      
      table_merge<-merge(table_sorted, table_mean, by = "port")
```


### Находим минимальную разницу между количеством байт и средним значением байт по каждому порту. Чем меньше значение, тем больше данных ip отправил.


```{r}
    res_ip<-filter(table_merge,str_detect(src,"13.37.84.125",negate=TRUE)&str_detect(src,"12.55.77.96",negate=TRUE))
    res_ip$difference <- res_ip$bytes - res_ip$port_mean
    min_diff_index <- which.min(res_ip$difference)

    min_diff_row <- res_ip[min_diff_index,]
      min_diff_row%>%head(1)%>%
      select(src)
```


### Ответ: 15.94.59.123

